<?php

/**
 * @file
 * Provides an additional config form for theme settings.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function yg_logistics_form_system_theme_settings_alter(array &$form, FormStateInterface $form_state) {
  $form['yg_logistics_settings'] = [
    '#type' => 'details',
    '#title' => t('YG Logistics Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'bootstrap',
    '#weight' => 10,
  ];
  $form['yg_logistics_settings']['footer']['copyright'] = [
    '#type' => 'text_format',
    '#title' => t('Copyrights'),
    '#default_value' => theme_get_setting('copyright')['value'],
    '#description'   => t("Please enter the copyright content here."),
  ];
  // banner-image.
  $form['yg_logistics_settings']['banner'] = [
    '#type' => 'fieldset',
    '#title' => t('Background Image'),
    '#open' => FALSE,
  ];
  $form['yg_logistics_settings']['banner']['bg-image'] = [
    '#type' => 'managed_file',
    '#title'    => t('Image'),
    '#default_value' => theme_get_setting('bg-image'),
    '#upload_location' => 'public://',
    '#description' => t('Choose your background image for Login, 404,403 pages'),
  ];
  // Social links.
  $form['yg_logistics_settings']['social_links'] = [
    '#type' => 'details',
    '#title' => t('Social Links'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['yg_logistics_settings']['social_links']['social_title'] = [
    '#type' => 'textfield',
    '#title' => t('Social Links Title'),
    '#description' => t('Please enter footer social title'),
    '#default_value' => theme_get_setting('social_title'),
    '#required' => TRUE,
  ];
  $form['yg_logistics_settings']['social_links']['facebook_url'] = [
    '#type' => 'textfield',
    '#title' => t('Facebook'),
    '#description' => t('Please enter your facebook url'),
    '#default_value' => theme_get_setting('facebook_url'),
  ];
  $form['yg_logistics_settings']['social_links']['twitter_url'] = [
    '#type' => 'textfield',
    '#title' => t('Twitter'),
    '#description' => t('Please enter your twitter url'),
    '#default_value' => theme_get_setting('twitter_url'),
  ];
  $form['yg_logistics_settings']['social_links']['youtube_url'] = [
    '#type' => 'textfield',
    '#title' => t('Youtube'),
    '#description' => t('Please enter your youtube url'),
    '#default_value' => theme_get_setting('youtube_url'),
  ];
  // Footer-contact-details.
  $form['yg_logistics_settings']['contact'] = [
    '#type' => 'details',
    '#title' => t('Contact Info'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['yg_logistics_settings']['contact']['contact_title'] = [
    '#type' => 'textfield',
    '#title' => t('Contact Title'),
    '#description' => t('Please enter footer contact title'),
    '#default_value' => theme_get_setting('contact_title'),
    '#required' => TRUE,
  ];
  $news_post_desc = theme_get_setting('address');
  $form['yg_logistics_settings']['contact']['address'] = [
    '#type' => 'text_format',
    '#title' => t('Address'),
    '#description' => t('Please enter address'),
    '#default_value' => $news_post_desc['value'],
    '#foramt'        => $news_post_desc['format'],
  ];
  $form['yg_logistics_settings']['contact']['email'] = [
    '#type' => 'email',
    '#title' => t('Email'),
    '#description' => t('Please enter email-id'),
    '#default_value' => theme_get_setting('email'),
  ];
  $form['yg_logistics_settings']['contact']['phone'] = [
    '#type' => 'textfield',
    '#title' => t('Phone Number'),
    '#description' => t('Please enter phone number'),
    '#default_value' => theme_get_setting('phone'),
  ];
}
